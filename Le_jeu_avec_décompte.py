import random
import time
import sys
import threading

class Question:
    def __init__(self, question, reponse, options):
        self.question = question 
        self.reponse = reponse
        self.options = options

def melanger_options(questions):
    for question in questions:
        random.shuffle(question.options)

temps = int(input("Veuillez entrer le temps du compte à rebours du quizz:  "))
LIMIT_TEMPS_PAR_QUESTION = temps

def lancer_quizz(questions):
    print("\t\t\t\t\t\t\t\t||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||")
    print("\t\t\t\t\t\t\t\t||                  1...2...3...QUESTION !!!                  ||")
    print("\t\t\t\t\t\t\t\t||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||\n\n")
    nom_utilisateur = input("Veuillez entrer votre pseudo : ")
    print("Merci, vous pouvez commencer maintenant\n\n")
    score = 0

    for numéro_question, question in enumerate(questions, start=1):
        
        # Afficher la question
        print(f"Question {numéro_question}:\n\n|||||||||||||||||||||||||||||||||||||||||\n{question.question}\n|||||||||||||||||||||||||||||||||||||||||")
        # Afficher les options de réponse
        for i, option in enumerate(question.options, start=1):
            print(f"({i}) {option}")
            
            
            

        temps_ecoule = False
        debut_temps = time.time()
        temps_limite = LIMIT_TEMPS_PAR_QUESTION

        # Affichage du décompte du temps
        while temps_limite > 0:
            sys.stdout.write(f"\rIl reste : {temps_limite}s |  Veuillez entrer votre réponse:  ")
            sys.stdout.flush()
            time.sleep(1) #vitesse du décompte
            temps_limite -= 1

            if temps_limite == 0:
                temps_ecoule = True

        sys.stdout.write("\r")  # Efface la ligne du décompte du temps
        

    
        # Validation de la réponse utilisateur après le décompte du temps
        if temps_ecoule:
            print("\nDommage, le temps est écoulé ! Sois plus rapide la prochaine\n")
        else:
            while True:
                reponse_utilisateur = input("Veuillez entrer le numéro de votre réponse correcte : ")
                if reponse_utilisateur.isdigit() and 1 <= int(reponse_utilisateur) <= len(question.options):
                    break
                else:
                    print("Veuillez entrer un numéro valide.")

        if question.options[int(reponse_utilisateur) - 1] == question.reponse and not temps_ecoule:
            score += 10
            print(f"Vrai, tu gagnes 10 points!\n")
        elif not temps_ecoule:
            print(f"Même ma grand-mère le sait, 0 point pour toi!\n")

    if score == 100:
        print(f"\t\t\t\tHey GG {nom_utilisateur}, vous avez terminé et gagné le |1,2,3 Questions|!!! Votre score est de {score} ")
    elif score < 31:
        print(f"\t\t\t\tHey {nom_utilisateur}, comment pouvez-vous être aussi nul? Vous avez {score} points")
    elif score > 200:
        print(f"\t\t\t\t Impossible!!! {nom_utilisateur}, vous avez terminé le |1,2,3 Questions|, et votre score est de ... WTF ?! {score} points!!! Excellent!!")

    rejouer = input("Voulez-vous rejouer ? (oui/non) ").lower()
    if rejouer == 'oui':
        lancer_quizz(questions)
    else:
        exit()

Questions_et_reponses_faciles = [
    Question("Quelle est la capitale de la France ?", "Paris", ["Paris", "Londres", "Berlin", "Madrid"]),
    Question("Quelle est la couleur du ciel ?", "Bleu", ["Rouge", "Bleu", "Vert", "Jaune"]),
    Question("Combien de doigts a une main ?", "Cinq", ["Trois", "Cinq", "Sept", "Neuf"]),
    Question("Quel est le mois de l'année avec 28 jours ?", "Février", ["Janvier", "Février", "Mars", "Avril"]),
]

Questions_et_reponses_moyennes = [
    Question("Quel est le plus grand océan du monde ?", "Océan Pacifique", ["Océan Atlantique", "Océan Indien", "Océan Arctique", "Océan Pacifique"]),
    Question("Quelle est la capitale du Canada ?", "Ottawa", ["Toronto", "Vancouver", "Ottawa", "Montréal"]),
    Question("Qui a écrit la pièce de théâtre 'Roméo et Juliette' ?", "William Shakespeare", ["Jane Austen", "Charles Dickens", "William Shakespeare", "Emily Brontë"]),
    Question("Combien de planètes composent notre système solaire ?", "8", ["7", "8", "9", "10"]),
    Question("Quel est le plus grand mammifère terrestre ?", "Éléphant d'Afrique", ["Rhinocéros", "Girafe", "Éléphant d'Asie", "Éléphant d'Afrique"]),  
]

Questions_et_reponses_difficiles = [
    Question("Quelle est la vitesse de la lumière en mètres par seconde ?", "299,792,458", ["150,000,000", "299,792,458", "450,000,000", "200,000,000"]),
    Question("Quelle est la température de fusion du tungstène en degrés Celsius ?", "3,422 °C", ["1,550 °C", "3,422 °C", "5,678 °C", "2,200 °C"]),
    Question("Quelle est la période orbitale de la planète Pluton en années terrestres ?", "248", ["128", "248", "365", "432"]),
    Question("Combien d'années lumière nous séparent de l'étoile la plus proche, Proxima Centauri ?", "4.24", ["2.34", "4.24", "6.78", "8.55"]),
    Question("Qui a découvert la structure en double hélice de l'ADN avec Francis Crick en 1953 ?", "James Watson", ["Rosalind Franklin", "James Watson", "Linus Pauling", "Maurice Wilkins"]),
]

toutes_les_questions = [Questions_et_reponses_faciles, Questions_et_reponses_moyennes, Questions_et_reponses_difficiles]
liste_questions_choisie = random.choice(toutes_les_questions)
lancer_quizz(liste_questions_choisie)
